Purpose
=======
This module creates properly displayed fields for data saved using the Tripal Contact Profile module (TCP)
The list of fields include:
First Name
Last Name
Species of Interest (pulls from TCP Genus)
Websites (pulls from TCP Url field)
Publications (pulls from TCP DOI field)


Installation
============

Copy module files to /sites/all/modules/tripal_contact_profile_display_fields