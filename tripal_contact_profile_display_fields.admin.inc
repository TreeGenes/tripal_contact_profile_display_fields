<?php

function tripal_contact_profile_display_fields_page_admin() {
	
	/*
	$form['vertical_tabs'] = array(
		'#type' => 'vertical_tabs'
	);
	

	$form['fieldset_settings'] = array(
		'#type' => 'fieldset',
		'#title' => t('Settings'),
		//'#group' => 'vertical_tabs',
	);	
	*/
	
	$form['tcpdf_hide_empty_display_fields'] = array(
		'#type' => 'checkbox',
		'#title' => t('Hide empty display fields'),
		'#description' => t('Hide empty display fields - you must clear cache for this to work'),
		'#default_value' => variable_get('tcpdf_hide_empty_display_fields', 0),
	);	


	return system_settings_form($form);		
	
}

?>